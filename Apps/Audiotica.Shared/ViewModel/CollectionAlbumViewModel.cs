﻿#region

using System.Linq;
using Windows.UI.Xaml.Controls;
using Audiotica.Data.Collection;
using Audiotica.Data.Collection.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

#endregion

namespace Audiotica.ViewModel
{
    public class CollectionAlbumViewModel : ViewModelBase
    {
        private readonly ICollectionService _service;
        private Album _album;
        private readonly RelayCommand<ItemClickEventArgs> _songClickCommand;

        public CollectionAlbumViewModel(ICollectionService service)
        {
            _service = service;
            _songClickCommand = new RelayCommand<ItemClickEventArgs>(SongClickExecute);
            MessengerInstance.Register<GenericMessage<long>>(this, "album-coll-detail-id", ReceivedId);

            if (IsInDesignMode)
                SetAlbum(0);
        }

        private void ReceivedId(GenericMessage<long> obj)
        {
            SetAlbum(obj.Content);
        }

        private async void SongClickExecute(ItemClickEventArgs e)
        {
            var song = e.ClickedItem as Song;
            var queueSong = _album.Songs.ToList();
            await CollectionHelper.PlaySongsAsync(song, queueSong);
        }

        public Album Album
        {
            get { return _album; }
            set { Set(ref _album, value); }
        }

        public RelayCommand<ItemClickEventArgs> SongClickRelayCommand
        {
            get { return _songClickCommand; }
        }

        public void SetAlbum(long id)
        {
            Album = _service.Albums.FirstOrDefault(p => p.Id == id);
        }
    }
}